package com.thuongnth.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.etUsername)
    EditText etUsername;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.cbRememberLogin)
    CheckBox cbRememberLogin;
    @BindView(R.id.btnLogin)
    TextView btnLogin;
    @BindView(R.id.btnNavigateSignUp)
    TextView btnNavigateSignUp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.cbRememberLogin, R.id.btnLogin, R.id.btnNavigateSignUp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cbRememberLogin:
                break;
            case R.id.btnLogin:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btnNavigateSignUp:
                startActivity(new Intent(this, RegisterActivity.class));
                finish();
                break;
        }
    }
}
